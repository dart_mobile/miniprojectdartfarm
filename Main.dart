import 'dart:io';
import 'Farm.dart';

void main(List<String> args) {
  clearScreen();
  Farm garden = new Farm();
  int level = garden.getLevel;
//print welcom and game detail.
  garden.randomNewQuest();
  garden.printWelcome();
  garden.printdetail();
  var nowLv = garden.getLevel;
  var count = 0;

  wait5Sec();
  clearScreen();
  print("Your farm level is $nowLv");
  garden.showQuest();
  showSelctMenu();

  var taskGet;
  int? selectHarvest;
  int selectmenu = int.parse(stdin.readLineSync()!);
  while (selectmenu != 8) {
    switch (selectmenu) {
      case 1:
//send
        if (garden.silo.isNotEmpty == true) {
          garden.truck.pickUp(garden.silo);
          if (garden.checkTailNotEmptyTail() == true) {
            if (garden.checkQuestSuccess() == true) {
              count++;
              garden.sendQuest();
              garden.randomNewQuest();
              garden.showQuest();
              if (count == 2) {
                nowLv++;
                count = 0;
                print("Your farm level is $nowLv");
              }
            } else {
              taskGet = garden.getTask;
              print("you must have $taskGet");
              clearScreen();
            }
          } else {
            print("Your truck is empty, have nothing send to quest");
            wait2Sec();
            clearScreen();
          }
        } else {
          print("Your silo is empty, you have to plant and harvest vegetable");
          wait2Sec();
          clearScreen();
        }
        break;
      case 2:
//plant
        clearScreen();
        garden.plant();
        break;
      case 3:
//harvest
        if (garden.plot.isEmpty) {
          print("Your plot is empty, you have not any plant to harvest.");
          wait2Sec();
          clearScreen();
        } else {
          garden.showplot();
          print("Select vegetable you want to harvest.");
          selectHarvest = int.parse(stdin.readLineSync()!);
          selectHarvest -= 1;
          garden.harvest(selectHarvest);
          wait2Sec();
          clearScreen();
        }
        break;
      case 4:
//show garden plot
        garden.showplot();
        wait2Sec();
        clearScreen();
        break;
      case 5:
//show task
        garden.showQuest();
        wait2Sec();
        clearScreen();
        break;
//show trunk
      case 6:
        garden.truck.showTrunk();
        wait2Sec();
        clearScreen();
        break;
//show silo
      case 7:
        garden.showSilo();
        wait2Sec();
        clearScreen();
        break;
    }
    showSelctMenu();
    selectmenu = int.parse(stdin.readLineSync()!);
    if (selectmenu == 8) {
      clearScreen();
      showWarning();
      var exit = stdin.readLineSync()!;
      if (exit == "Y") {
        break;
      } else if (exit == "N") {
        selectmenu = 0;
      }
    }
  }
}

void wait5Sec() {
  sleep(const Duration(seconds: 5));
}

wait2Sec() {
  sleep(const Duration(seconds: 2));
}

clearScreen() {
  for (int i = 0; i < stdout.terminalLines; i++) {
    stdout.writeln();
  }
}

showSelctMenu() {
  print("Crops");
  print("     1. Send a crops.");
  print("     2. Plant a crops.");
  print("     3. Harvest a crops.");

  print("Show");
  print("     4. Show garden plots.");
  print("     5. Show task");
  print("     6. Show trunk");
  print("     7. Show silo");
  print("     8. Exit.");
}

bar() {
  print("----------------------");
}

showWarning() {
  print("Are you sure to exit your farm??");
  print(" **you progess will be lost**");
  bar();
  print("If you want to exit yes[Y] / no[N]");
}
