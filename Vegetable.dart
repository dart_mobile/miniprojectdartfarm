import 'Harvest.dart';
import 'plant.dart';

class Vegetable extends Plant with Harvest{
  String? name;
  int? timeGrow;
  List vegetsList = ['Cucumber', 'Onion', 'Carrot', 'Lettuce', 'Tomato'];

  Vegetable() {
    this.name = name;
    this.timeGrow = timeGrow;
    this.vegetsList = vegetsList;
  }
  @override
  harvest(name) {
    print(name.toString() + " has harvested  in your farm");
  }

  @override
  plant(name) {
    print(name.toString() + " has planted form  your farm");
  }

  @override
  printlist() {
    print("All vegetables list here!!!");
    int count = 1;
    for (String fr in vegetsList) {
      print(count.toString() + ". " + fr.toString());
      count++;
    }
  }
}
