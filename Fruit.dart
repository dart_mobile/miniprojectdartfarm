import 'Harvest.dart';
import 'plant.dart';

class Fruit extends Plant with Harvest {
  String? name;
  int? timeGrow;
  var fruitsList = ['Orange', 'Grape', 'Apple', 'Peach', 'Banana'];
  Fruit() {
    this.name = name;
    this.timeGrow = timeGrow;
    this.fruitsList = fruitsList;
  }

  @override
  harvest(name) {
    print(name.toString() + " has harvested in your farm");
  }

  @override
  plant(name) {
    print(name.toString() + " has planted form your farm");
  }

  @override
  printlist() {
    int count = 1;
    print("All fruits list here!!!");
    for (String fr in fruitsList) {
      print(count.toString() + ". " + fr.toString());
      count++;
    }
  }
}
