import 'dart:math';

class Quest {
  String? nameTask;
  int? point = 1;
  int? rdTask;
  int? rdPoint;
  List<String> task = [];
  List<String> listOfTask = [
    'Cucumber',
    'Orange',
    'Peach',
    'Onion',
    'Grape',
    'Carrot',
    'Apple',
    'Lettuce',
    'Tomato',
    'Banana'
  ];

  Quest() {
    this.nameTask = nameTask;
    this.point = point;
    this.listOfTask = listOfTask;
  }

  randomTask(int level) {
    List<String> randomTask = [];
    for (var i = 0; i < level; i++) {
      rdTask = Random().nextInt(9);
      randomTask.add(listOfTask.elementAt(rdTask!));
    }
    task = randomTask.toSet().toList();
  }

  randomPoint(int level) {
    rdPoint = Random().nextInt(level) + 1;
    point = rdTask;
  }

  get getTask {
    return task;
  }
}
