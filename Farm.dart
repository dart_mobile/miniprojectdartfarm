import 'dart:io';
import 'Fruit.dart';
import 'Main.dart';
import 'Quest.dart';
import 'Truck.dart';
import 'Vegetable.dart';

class Farm {
  String name = "Dart Farm";
  int size = 5;
  int level = 1;
  List<String> silo = [];
  List<String> plot = [];
  List<String> task = [];
  // List<int> water = [];
  int? select;
  int choose = -1;
  String? playerName;

  Vegetable vegets = new Vegetable();
  Fruit fruit = new Fruit();
  Truck truck = new Truck();
  Quest quest = new Quest();
  get getLevel {
    return level;
  }

  get getTask {
    return task;
  }

  randomNewQuest() {
    quest.randomTask(level);
    quest.randomPoint(level);
    task = quest.getTask;
  }

  showQuest() {
    print("Now your task is $task");
  }

  bool checkQuestSuccess() {
    print("Now your task is $task");
    List<bool> listBool = [];
    int? taskLen = task.length;
    int? siloLen = silo.length;
    for (var i = 0; i < taskLen; i++) {
      for (var j = 0; j < siloLen; j++) {
        if (task.elementAt(i) == silo.elementAt(j)) {
          listBool.add(true);
          break;
        }
      }
    }
    if (listBool.length == task.length) {
      for (var i = 0; i < taskLen; i++) {
        for (var j = 0; j < siloLen; j++) {
          if (task.elementAt(i) == silo.elementAt(j)) {
            silo.removeAt(j);
            break;
          }
        }
      }
      return true;
    }
    return false;
  }

  bool checkTailNotEmptyTail() {
    List<String> crops = truck.getTailgate;
    if (crops.isNotEmpty) {
      print("You have $crops in your trunk");
      return true;
    }
    return false;
  }

  setLevel(int level) {
    this.level = level;
  }

  harvest(int index) {
    if (plot.elementAt(index) == 'Cucumber' ||
        plot.elementAt(index) == 'Onion' ||
        plot.elementAt(index) == 'Carrot' ||
        plot.elementAt(index) == 'Lettuce' ||
        plot.elementAt(index) == 'Tomato') {
      String fromPlot = plot.elementAt(index);
      switch (fromPlot) {
        case 'Cucumber':
          silo.add("Cucumber");
          break;
        case 'Onion':
          silo.add("Onion");
          break;
        case 'Carrot':
          silo.add("Carrot");
          break;
        case 'Lettuce':
          silo.add("Lettuce");
          break;
        case 'Tomato':
          silo.add("Tomato");
          break;
      }
      vegets.harvest(plot.elementAt(index));
      showSilo();
    } else if (plot.elementAt(index) == 'Orange' ||
        plot.elementAt(index) == 'Grape' ||
        plot.elementAt(index) == 'Apple' ||
        plot.elementAt(index) == 'Peach' ||
        plot.elementAt(index) == 'Banana') {
      String fromPlot = plot.elementAt(index);
      switch (fromPlot) {
        case 'Orange':
          silo.add("Orange");
          break;
        case 'Grape':
          silo.add("Grape");
          break;
        case 'Apple':
          silo.add("Apple");
          break;
        case 'Peach':
          silo.add("Peach");
          break;
        case 'Banana':
          silo.add("Banana");
          break;
      }
      fruit.harvest(plot.elementAt(index));
      showSilo();
    }
    plot.removeAt(index);
  }

  plant() {
    if (plot.length < 5) {
      selectplant();
      select = int.parse(stdin.readLineSync()!);
      clearScreen();
      // print("you select $select");
      switch (select) {
        case 1:
          showSelectList();
          vegets.printlist();
          print("6. Back");
          addVegettable();
          showUpdateNowPlot();
          showplot();
          wait2Sec();
          clearScreen();
          break;
        case 2:
          showSelectList();
          fruit.printlist();
          print("6. Back");
          addFruit();
          showUpdateNowPlot();
          showplot();
          wait2Sec();
          clearScreen();
          break;
        case 3:
          clearScreen();
          break;
      }
    } else {
      print(plot);
      print("your garden is Empty, please harvest yoou crop before.");
    }
  }

  selectplant() {
    print("Select vegetables you want to plant.");
    print("1. Vegetable");
    print("2. Fruits");
    print("3. Back");
  }

  addFruit() {
    choose = int.parse(stdin.readLineSync()!);
    choose = choose - 1;
    switch (choose) {
      case 0:
        plot.add("Orange");
        break;
      case 1:
        plot.add("Grape");
        break;
      case 2:
        plot.add("Apple");
        break;
      case 3:
        plot.add("Peach");
        break;
      case 4:
        plot.add("Banana");
        break;
      case 5:
        plant();
        break;
    }
  }

  addVegettable() {
    choose = int.parse(stdin.readLineSync()!);
    choose = choose - 1;
    switch (choose) {
      case 0:
        plot.add("Cucumber");
        break;
      case 1:
        plot.add("Onion");
        break;
      case 2:
        plot.add("Carrot");
        break;
      case 3:
        plot.add("Lettuce");
        break;
      case 4:
        plot.add("Tomato");
        break;
      case 5:
        plant();
        print("asdasdsad");
        clearScreen();
        break;
    }
  }

  showUpdateNowPlot() {
    print("This is vegetable in your garden plot");
  }

  showplot() {
    if (plot.isEmpty) {
      print("your garden plot is empty");
    } else {
      print(plot);
    }
  }

  finishQuest() {
    truck.send();
  }

  printWelcome() {
    print("Hello! Welcome to $name .");
    print("First Let's enter your name : ");
    playerName = stdin.readLineSync();
    bar();
    print(
        "Hello $playerName, Your farm now Level $level. I wish you have fun^^");
  }

  printdetail() {
    print("First, You have 5 plot to plant any drop you want.");
    print("Second, They might need water to grow.");
    print(
        "Finally, When you send your crops you must wait 10 second for send your crop.");
  }

  showSelectList() {
    print("Which one vegetables you want to plant?");
  }

  showSilo() {
    if (silo.isEmpty == true) {
      print("You have nothing in your trunk.");
    } else {
      print("Your crops has add to silo. Now your silo $silo");
    }
  }

  sendQuest() {
    truck.send();
  }
}
